﻿namespace FunWithLine
{
    using System;

    public class TestCommodityCode
    {
        public void Run()
        {
            Console.Write("Enter commodity code: ");
            string codeToTest = Console.ReadLine();

            Console.Write("Enter number for 'maxLength': ");
            int maxLength = int.Parse(Console.ReadLine());

            var result = MnemonicToStringNumber(codeToTest, maxLength);

            Console.WriteLine($"Result: {result}");
        }

        /// <summary>
        /// Written by Dũng Nguyễn - SD2507
        /// </summary>
        /// <param name="mnemonic"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string MnemonicToStringNumber(string mnemonic, int maxLength = 4)
        {
            if (String.IsNullOrWhiteSpace(mnemonic)) return "";

            string[] arr = mnemonic.Split('.');
            string result = "";

            foreach (string part in arr)
            {
                if (part.Length < maxLength)
                    result += part.PadLeft(maxLength, '0');
                else
                    result += part;
            }

            return result;
        }
    }
}
