﻿namespace FunWithLine
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    public class LongestWord
    {
        public void Run()
        {
            Console.Write("Enter your sentences: ");
            var results = GetLongestWord(Console.ReadLine()?.Split(' '));

            Console.WriteLine($"The longest word is/are: ");

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        public List<string> GetLongestWord(string[] words)
        {
            var sortedWords = words.OrderByDescending(x => x.Length).ToArray();

            var result = new List<string>();

            var totalSortedWords = sortedWords.Length;

            if (totalSortedWords > 2)
            {
                for (var index = 0; index < totalSortedWords; index++)
                {
                    result.Add(sortedWords[index]);

                    if (sortedWords[index].Length > sortedWords[index+1].Length)
                    {
                        break;
                    }
                }
            }
            else
            {
                result.Add(sortedWords.FirstOrDefault());
            }

            return result;
        }
    }
}